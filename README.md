![description](http://lutim.cpy.re/2RK8XFaj.png)

**Developed with :heart: by [Framasoft](https://framasoft.org/en/)**

## Introduction

Framaforms is an online forms and surveys service. Registered users are allowed to create forms, share them, collect and visualize results through a simple interface. Detailed features and usage can be found [here](https://docs.framasoft.org/fr/framaforms/) (in French).
See why we created it ([#1](https://framablog.org/2016/10/05/framaforms-noffrez-plus-les-reponses-que-vous-collectez-a-google/) and [#2](https://framablog.org/2016/10/05/en-savoir-un-peu-plus-sur-le-projet-framaforms/) in French) for further general information.

Framaforms is based on Drupal 7 and makes extensive use of the `webform` modules as well as others.

## Installation :fire:

You don't need superpowers to install Framaforms. If you've already installed Drupal, you'll be on familiar ground. If you haven't, don't you worry, you only need basic knowledge.

Follow detailed instructions [here](https://framagit.org/framasoft/framaforms/-/wikis/Installing-Framaforms-through-the-installation-profile).

## Homemade tweaking of Drupal :construction_site: 
As mentioned above, Framaforms is mainly Drupal, and we try as much as possible not to mess with the upstream code.

However, due to our environment and needs, a few custom modifications have been made to the original code. **If you ever need to update the Drupal core or a module** and that update is not commited to the code on the repo, you'll find all the instructions [on this wiki page](https://framagit.org/framasoft/framaforms/-/wikis/modifications).

## Resources & support :mailbox:
Technical details can be found [on the wiki](https://framagit.org/framasoft/framaforms/-/wikis/home).

For community support, please refer to [our Discourse](https://framacolibri.org) (mostly in French).

*Framaforms code is under GPLv2, [Framasoft graphical elements](https://framasoft.org/en/graphics/) are under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)*